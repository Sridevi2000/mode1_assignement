//1.Write a SQL statement to create a simple table countries including columns country_id,country_name and region_id.

mysql> create table countries(country_id int,country_name varchar(30),region_id int);
Query OK, 0 rows affected (3.07 sec)

mysql> describe countries;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| country_id   | int         | YES  |     | NULL    |       |
| country_name | varchar(30) | YES  |     | NULL    |       |
| region_id    | int         | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.11 sec)

//2.Write a SQL statement to create a table countries set a constraint NULL.

mysql> create table if not exists countries1(country_id int not null , country_name varchar(30) not null, region_id int not null);
Query OK, 0 rows affected (0.91 sec)

mysql> describe countries1;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| country_id   | int         | NO   |     | NULL    |       |
| country_name | varchar(30) | NO   |     | NULL    |       |
| region_id    | int         | NO   |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.17 sec)

mysql> create table if not exists countries(country_id int not null , country_name varchar(30) not null, region_id int not null);
Query OK, 0 rows affected, 1 warning (0.07 sec)

mysql> describe countries1;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| country_id   | int         | NO   |     | NULL    |       |
| country_name | varchar(30) | NO   |     | NULL    |       |
| region_id    | int         | NO   |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> describe countries;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| country_id   | int         | YES  |     | NULL    |       |
| country_name | varchar(30) | YES  |     | NULL    |       |
| region_id    | int         | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

//3.create table locations including columns.

mysql> create table locations(LOCATION_ID decimal(4,0), STREET_ADdRESS varchar(40),POSTAL_CODE varchar(12), CITY varchar(30), STATE_PROVINCE varchar(25),COUNTRY_ID varchar(2));
Query OK, 0 rows affected (1.89 sec)

mysql> desc locations;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| LOCATION_ID    | decimal(4,0) | YES  |     | NULL    |       |
| STREET_ADdRESS | varchar(40)  | YES  |     | NULL    |       |
| POSTAL_CODE    | varchar(12)  | YES  |     | NULL    |       |
| CITY           | varchar(30)  | YES  |     | NULL    |       |
| STATE_PROVINCE | varchar(25)  | YES  |     | NULL    |       |
| COUNTRY_ID     | varchar(2)   | YES  |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
6 rows in set (0.10 sec)

//4.
Alter Table
 4.1.Write a SQL statement to rename the table countries to country_new.
    mysql> rename table countries to country_new;
    Query OK, 0 rows affected (0.83 sec)
    mysql> alter table country_new rename to Countries;
    Query OK, 0 rows affected (0.93 sec)
 4.2.Write a SQL statement to add a columns ID as the first column of the table locations.
      mysql> alter table locations add ID int First;
       Query OK, 0 rows affected (3.05 sec)
        Records: 0  Duplicates: 0  Warnings: 0
4.3Write a SQL statement to add a column region_id after state_province to the table locations
        mysql> alter table locations add region_id int after state_province;
        Query OK, 0 rows affected (2.85 sec)
        Records: 0  Duplicates: 0  Warnings: 0
    mysql> describe locations;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| ID             | int          | YES  |     | NULL    |       |
| LOCATION_ID    | decimal(4,0) | YES  |     | NULL    |       |
| STREET_ADdRESS | varchar(40)  | YES  |     | NULL    |       |
| POSTAL_CODE    | varchar(12)  | YES  |     | NULL    |       |
| CITY           | varchar(30)  | YES  |     | NULL    |       |
| STATE_PROVINCE | varchar(25)  | YES  |     | NULL    |       |
| region_id      | int          | YES  |     | NULL    |       |
| COUNTRY_ID     | varchar(2)   | YES  |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
8 rows in set (0.06 sec)


4.4.Write a SQL statement change the data type of the column country_id to integer in the table locations
    
mysql> alter table locations modify country_id int;
Query OK, 0 rows affected (3.70 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe locations;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| ID             | int          | YES  |     | NULL    |       |
| LOCATION_ID    | decimal(4,0) | YES  |     | NULL    |       |
| STREET_ADdRESS | varchar(40)  | YES  |     | NULL    |       |
| POSTAL_CODE    | varchar(12)  | YES  |     | NULL    |       |
| CITY           | varchar(30)  | YES  |     | NULL    |       |
| STATE_PROVINCE | varchar(25)  | YES  |     | NULL    |       |
| region_id      | int          | YES  |     | NULL    |       |
| country_id     | int          | YES  |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
8 rows in set (0.23 sec)


Insert Table

1.Write a SQL statement to insert 3 rows by a single insert statement
mysql> insert into countries values(1,'India',34),(2,'usa',76),(3,'canada',55);
Query OK, 3 rows affected (0.17 sec)
Records: 3  Duplicates: 0  Warnings: 0
mysql> select *from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | India        |        34 |
|          2 | usa          |        76 |
|          3 | canada       |        55 |
+------------+--------------+-----------+
3 rows in set (0.00 sec)

2.Write a SQL statement to insert rows into the table countries in which the value of country_id column will be unique and auto incremented

mysql> alter table countries modify column country_id int  unique auto_increment;
Query OK, 3 rows affected (3.02 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> desc countries;
+--------------+-------------+------+-----+---------+----------------+
| Field        | Type        | Null | Key | Default | Extra          |
+--------------+-------------+------+-----+---------+----------------+
| country_id   | int         | NO   | PRI | NULL    | auto_increment |
| country_name | varchar(30) | YES  |     | NULL    |                |
| region_id    | int         | YES  |     | NULL    |                |
+--------------+-------------+------+-----+---------+----------------+
3 rows in set (0.07 sec)


3.Write a SQL statement to insert rows only for country_id and country_name
  mysql> insert into countries (country_id , country_name) values(5,'africa');
Query OK, 1 row affected (0.09 sec)

mysql> select * from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | India        |        34 |
|          2 | usa          |        76 |
|          3 | canada       |        55 |
|          5 | africa       |      NULL |
+------------+--------------+-----------+
4 rows in set (0.00 sec)

Update Table

Employee table creation:

 

mysql> create table employees(employee_id int,first_name varchar(20),email varchar(20),salary decimal(5,2),commission_pc decimal(2,2),department_id int);
Query OK, 0 rows affected, 1 warning (0.90 sec)

 

mysql> insert into employees(103,'Alexander','AHUNOLD',9000.00,0.00,60),(104,'Bruce','BERNST',6000.00,0.00,60),(105,'David','DAUSTIN',4800.00,0.00,60);
Query OK, 3 rows affected (0.01 sec)
Records: 3  Duplicates: 0  Warnings: 0

 

mysql> insert into employees values(103,'Alexander','AHUNOLD',9000.00,0.00,60),(104,'Bruce','BERNST',6000.00,0.00,60),(105,'David','DAUSTIN',4800.00,0.00,60);
Query OK, 3 rows affected (0.00 sec)
Records: 3  Duplicates: 0  Warnings: 0

 

mysql> insert into employees values(106,'Valli','VPATABAL',4200.00,0.00,60),(107,'Diana','DLORENTZ',12008.00,0.00,110),(205,'Shelley','SHIGGINS',8300.00,0.00,110),
    -> (206,'William','WGIETZ',8300.00,0.00,110);
Query OK, 4 rows affected (0.00 sec)

 

mysql> select * from employees;

 

+-------------+------------+----------+----------+---------------+---------------+
| employee_id | first_name | email    | salary   | commission_pc | department_id |
+-------------+------------+----------+----------+---------------+---------------+
|         100 | Steven     | SKING    | 24000.00 |             0 |            90 |
|         101 | Neena      | NKOCHHAR |  1700.00 |             0 |            90 |
|         102 | Lex        | LDEHAAN  | 17000.00 |             0 |            90 |
|         103 | Alexander  | AHUNOLD  |  9000.00 |             0 |            60 |
|         104 | Bruce      | BERNST   |  6000.00 |             0 |            60 |
|         105 | David      | DAUSTIN  |  4800.00 |             0 |            60 |
|         106 | Valli      | VPATABAL |  4200.00 |             0 |            60 |
|         107 | Diana      | DLORENTZ | 12008.00 |             0 |           110 |
|         205 | Shelley    | SHIGGINS |  8300.00 |             0 |           110 |
|         206 | William    | WGIETZ   |  8300.00 |             0 |           110 |
+-------------+------------+----------+----------+---------------+---------------+
10 rows in set (0.00 sec)

 

1.Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for all employees

 

Code:

 

mysql> update employees set email='not available',commission_pct=0.10 where commission_pct=0.00;
Query OK, 10 rows affected (0.09 sec)
Rows matched: 10  Changed: 10  Warnings: 0

 

mysql> select * from employees;
+-------------+------------+---------------+----------+----------------+---------------+
| employee_id | first_name | email         | salary   | commission_pct | department_id |
+-------------+------------+---------------+----------+----------------+---------------+
|         100 | Steven     | not available | 24000.00 |           0.10 |            90 |
|         101 | Neena      | not available |  1700.00 |           0.10 |            90 |
|         102 | Lex        | not available | 17000.00 |           0.10 |            90 |
|         103 | Alexander  | not available |  9000.00 |           0.10 |            60 |
|         104 | Bruce      | not available |  6000.00 |           0.10 |            60 |
|         105 | David      | not available |  4800.00 |           0.10 |            60 |
|         106 | Valli      | not available |  4200.00 |           0.10 |            60 |
|         107 | Diana      | not available | 12008.00 |           0.10 |           110 |
|         205 | Shelley    | not available |  8300.00 |           0.10 |           110 |
|         206 | William    | not available |  8300.00 |           0.10 |           110 |
+-------------+------------+---------------+----------+----------------+---------------+
10 rows in set (0.00 sec)
 
2.Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for those employees whose department_id is 110.
 
mysql> update employee set email='not available',commission_pct=0.10 where department_id=110;
Query OK, 2 rows affected (0.09 sec)
Rows matched: 2  Changed: 2  Warnings: 0

 

mysql> select * from employee;
+-------------+------------+---------------+----------+----------------+---------------+
| employee_id | first_name | email         | salary   | commission_pct | department_id |
+-------------+------------+---------------+----------+----------------+---------------+
|         100 | Steven     | SKING         | 24000.00 |           0.00 |            90 |
|         101 | Neena      | NKOCHHAR      |  1700.00 |           0.00 |            90 |
|         102 | Lex        | LDEHAAN       | 17000.00 |           0.00 |            90 |
|         103 | Alexander  | AHUNOLD       |  9000.00 |           0.00 |            60 |
|         104 | Bruce      | BERNST        |  6000.00 |           0.00 |            60 |
|         105 | David      | DAUSTIN       |  4800.00 |           0.00 |            60 |
|         106 | Valli      | VPATABAL      |  4200.00 |           0.00 |            60 |
|         107 | Diana      | not available | 12008.00 |           0.10 |           110 |
|         205 | Shelley    | not available |  8300.00 |           0.10 |           110 |
+-------------+------------+---------------+----------+----------------+---------------+
9 rows in set (0.00 sec)



1.Write a SQL statement to display all the information of all salesmen

mysql> create table salesman;
ERROR 4028 (HY000): A table must have at least one visible column.
mysql> create table salesman(salesman_id int, name varchar(30), city varchar(30), commission decimal(2,2));
Query OK, 0 rows affected (2.28 sec)

mysql> insert into salesman values(5001,'James Hoog','Newyork',0.15);
Query OK, 1 row affected (0.18 sec)

mysql> insert into salesman values(5002,'Nail knite','paris',0.13),(5005,'pit alex','London',0.11),(5006,'mc lyon','paris',0.14),(5003,'lauson hem','delhi',0.12),(5007,'paul adam','rome',0.13);
Query OK, 5 rows affected (0.14 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from salesman;
+-------------+------------+---------+------------+
| salesman_id | name       | city    | commission |
+-------------+------------+---------+------------+
|        5001 | James Hoog | Newyork |       0.15 |
|        5002 | Nail knite | paris   |       0.13 |
|        5005 | pit alex   | London  |       0.11 |
|        5006 | mc lyon    | paris   |       0.14 |
|        5003 | lauson hem | delhi   |       0.12 |
|        5007 | paul adam  | rome    |       0.13 |
+-------------+------------+---------+------------+
6 rows in set (0.00 sec)


2.Write a SQL statement to display specific columns like name and commission for all the salesmen.

mysql> select name,commission from salesman;
+------------+------------+
| name       | commission |
+------------+------------+
| James Hoog |       0.15 |
| Nail knite |       0.13 |
| pit alex   |       0.11 |
| mc lyon    |       0.14 |
| lauson hem |       0.12 |
| paul adam  |       0.13 |
+------------+------------+
6 rows in set (0.00 sec)

3.Write a SQL statement to display names and city of salesman, who belongs to the city of Paris

mysql> select name,city from salesman where city='paris';
+------------+-------+
| name       | city  |
+------------+-------+
| Nail knite | paris |
| mc lyon    | paris |
+------------+-------+
2 rows in set (0.04 sec)

4.Write a query to display the columns in a specific order like order date, salesman id, order number and purchase amount from for all the orders

mysql> select ord_date,salesman_id,ord_no,purch_amt from orders;
+------------+-------------+--------+-----------+
| ord_date   | salesman_id | ord_no | purch_amt |
+------------+-------------+--------+-----------+
| 2012-10-05 |        5002 |  70001 |    150.50 |
| 2012-09-10 |        5005 |  70009 |    270.65 |
| 2012-10-05 |        5001 |  70002 |     65.26 |
| 2012-08-17 |        5003 |  70004 |    110.50 |
| 2012-09-10 |        5002 |  70007 |    948.50 |
| 2012-07-27 |        5001 |  70005 |   2400.60 |
| 2012-09-10 |        5001 |  70008 |   5760.00 |
| 2012-10-10 |        5006 |  70010 |   1983.43 |
| 2012-10-10 |        5003 |  70003 |   2480.40 |
| 2012-06-27 |        5002 |  70012 |    250.45 |
| 2012-08-12 |        5007 |  70011 |     75.29 |
| 2012-04-25 |        5001 |  70013 |   3045.60 |
+------------+-------------+--------+-----------+
12 rows in set (0.00 sec)


5.Write a query which will retrieve the value of salesman id of all salesmen, getting orders from the customers in orders table without any repeats.


mysql> select distinct(salesman_id) from orders;
+-------------+
| salesman_id |
+-------------+
|        5002 |
|        5005 |
|        5001 |
|        5003 |
|        5006 |
|        5007 |
+-------------+
6 rows in set (0.05 sec)

6. Write a SQL statement to display all the information for those customers with a grade of 200

mysql> select * from customers where grade=200;
+-------------+---------------+------------+-------+-------------+
| customer_id | customer_name | city       | grade | salesman_id |
+-------------+---------------+------------+-------+-------------+
|        3005 | Graham Zusi   | California |   200 |        5002 |
|        3007 | Brad Davis    | New york   |   200 |        5001 |
|        3003 | Jozy Altidor  | Moscow     |   200 |        5007 |
+-------------+-----------


7.Write a SQL query to find all the products with a price between Rs.200 and Rs.600
 

mysql> select * from products where pro_price between 200 and 600;
+--------+------------------+-----------+---------+
| pro_id | pro_name         | pro_price | pro_com |
+--------+------------------+-----------+---------+
|    102 | Key board        |       450 |      16 |
|    103 | Zip drive        |       250 |      14 |
|    104 | Speaker          |       550 |      16 |
|    109 | Refill Cartridge |       350 |      13 |
|    110 | Mouse            |       250 |      12 |
+--------+------------------+-----------+---------+
5 rows in set (0.00 sec)



AGGREGATION FUNCTION:

1.Write a SQL statement to find the total purchase amount of all orders.

mysql> select sum(purch_amt) from orders;
+----------------+
| sum(purch_amt) |
+----------------+
|       17541.18 |
+----------------+
1 row in set (0.00 sec)

2.Write a SQL statement to find the average purchase amount of all orders

mysql> select avg(purch_amt) from orders;
+----------------+
| avg(purch_amt) |
+----------------+
|    1461.765000 |
+----------------+

3.Write a SQL statement to find the number of salesmen currently listing for all of their customers

mysql> select count(distinct(salesman_id)) from orders;
+------------------------------+
| count(distinct(salesman_id)) |
+------------------------------+
|                            6 |
+------------------------------+
1 row in set (0.00 sec)

4.Write a SQL statement know how many customer have listed their names.

mysql> select count(customer_name) from customers;
+----------------------+
| count(customer_name) |
+----------------------+
|                    8 |
+----------------------+
1 row in set (0.00 sec)

5.Write a SQL statement to get the maximum purchase amount of all the orders

mysql> select max(purch_amt) from orders;
+----------------+
| max(purch_amt) |
+----------------+
|        5760.00 |
+----------------+
1 row in set (0.00 sec)


RELATIONAL OPERATORS:

1.Write a query to display all customers with a grade above 100

mysql> select * from customers where grade>100;
+-------------+---------------+------------+-------+-------------+
| customer_id | customer_name | city       | grade | salesman_id |
+-------------+---------------+------------+-------+-------------+
|        3005 | Graham Zusi   | California |   200 |        5002 |
|        3001 | Brad Guzal    | London     |   600 |        5005 |
|        3004 | Fabian Johns  | Paris      |   300 |        5006 |
|        3007 | Brad Davis    | New york   |   200 |        5001 |
|        3008 | Julian Green  | London     |   300 |        5002 |
|        3003 | Jozy Altidor  | Moscow     |   200 |        5007 |
+-------------+---------------+------------+-------+-------------+
6 rows in set (0.00 sec)


2.Write a query statement to display all customers in New York who have a grade value above 100

mysql> select * from customers where city="New york" and grade>100;
+-------------+---------------+----------+-------+-------------+
| customer_id | customer_name | city     | grade | salesman_id |
+-------------+---------------+----------+-------+-------------+
|        3007 | Brad Davis    | New york |   200 |        5001 |
+-------------+---------------+----------+-------+-------------+
1 row in set (0.00 sec)


3.Write a SQL statement to display all customers, who are either belongs to the city New York or had a grade above 100


mysql> select * from customers where city="New york" or grade>100;
+-------------+---------------+------------+-------+-------------+
| customer_id | customer_name | city       | grade | salesman_id |
+-------------+---------------+------------+-------+-------------+
|        3002 | Nick Rimando  | New york   |   100 |        5001 |
|        3005 | Graham Zusi   | California |   200 |        5002 |
|        3001 | Brad Guzal    | London     |   600 |        5005 |
|        3004 | Fabian Johns  | Paris      |   300 |        5006 |
|        3007 | Brad Davis    | New york   |   200 |        5001 |
|        3008 | Julian Green  | London     |   300 |        5002 |
|        3003 | Jozy Altidor  | Moscow     |   200 |        5007 |
+-------------+---------------+------------+-------+-------------+
7 rows in set (0.00 sec)


4.Write a SQL statement to display either those orders which are not issued on date 2012-09-10 and issued by the salesman whose ID is 505 
and below or those orders which purchase amount is 1000.00 and below.
 

mysql> select * from orders where (not ord_date='2012-09-10' and salesman_id<=5005) or (purch_amt<=1000.00);
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70001 |    150.50 | 2012-10-05 |        3005 |        5002 |
|  70009 |    270.65 | 2012-09-10 |        3001 |        5005 |
|  70002 |     65.26 | 2012-10-05 |        3002 |        5001 |
|  70004 |    110.50 | 2012-08-17 |        3009 |        5003 |
|  70007 |    948.50 | 2012-09-10 |        3005 |        5002 |
|  70005 |   2400.60 | 2012-07-27 |        3007 |        5001 |
|  70003 |   2480.40 | 2012-10-10 |        3009 |        5003 |
|  70012 |    250.45 | 2012-06-27 |        3008 |        5002 |
|  70011 |     75.29 | 2012-08-17 |        3003 |        5007 |
|  70013 |   3045.60 | 2012-04-25 |        3002 |        5001 |
+--------+-----------+------------+-------------+-------------+
10 rows in set (0.00 sec)

SORTING and FILTERING:

1.Write a query in SQL to display the full name (first and last name), and salary for those employees who earn below 6000


mysql> select first_name,last_name,salary from employee where salary<6000;
+------------+-----------+---------+
| first_name | last_name | salary  |
+------------+-----------+---------+
| Neena      | NKOCHHAR  | 1700.00 |
| David      | DAUSTIN   | 4800.00 |
| Valli      | VPATABAL  | 4200.00 |
+------------+-----------+---------+
3 rows in set (0.00 sec)


2.Write a query in SQL to display the first and last name, and department number for all employees whose last name is "Ernst".


mysql> select first_name,last_name,department_id from employee where last_name='ERNST';
Empty set (0.00 sec)


mysql> select first_name,last_name,department_id from employee where last_name like '_ERNST';
+------------+-----------+---------------+
| first_name | last_name | department_id |
+------------+-----------+---------------+
| Bruce      | BERNST    |            60 |
+------------+-----------+---------------+
1 row in set (0.00 sec)


3.Write a query in SQL to display the full name (first and last),  salary, and department number for those employees whose first name does not containing 
the letter M and make the result set in ascending order by department number

mysql> select first_name,last_name,salary,department_id from employee where first_name not like '%m' order by department_id;
+------------+-----------+----------+---------------+
| first_name | last_name | salary   | department_id |
+------------+-----------+----------+---------------+
| Alexander  | AHUNOLD   |  9000.00 |            60 |
| Bruce      | BERNST    |  6000.00 |            60 |
| David      | DAUSTIN   |  4800.00 |            60 |
| Valli      | VPATABAL  |  4200.00 |            60 |
| Steven     | SKING     | 24000.00 |            90 |
| Neena      | NKOCHHAR  |  1700.00 |            90 |
| Lex        | LDEHAAN   | 17000.00 |            90 |
| Diana      | DLORENTZ  | 12008.00 |           110 |
| Shelley    | SHIGGINS  |  8300.00 |           110 |
+------------+-----------+----------+---------------+
9 rows in set (0.00 sec)

4.Write a query in SQL to display the full name (first and last name), and salary for all employees who does not earn any commission
 
mysql> select first_name,last_name,salary from employee where commission_pct=0;
+------------+-----------+----------+
| first_name | last_name | salary   |
+------------+-----------+----------+
| Steven     | SKING     | 24000.00 |
| Neena      | NKOCHHAR  |  1700.00 |
| Lex        | LDEHAAN   | 17000.00 |
| Alexander  | AHUNOLD   |  9000.00 |
| Bruce      | BERNST    |  6000.00 |
| David      | DAUSTIN   |  4800.00 |
| Valli      | VPATABAL  |  4200.00 |
| Diana      | DLORENTZ  | 12008.00 |
| Shelley    | SHIGGINS  |  8300.00 |
| William    | WGIETZ    |  8300.00 |
+------------+-----------+----------+
10 rows in set (0.00 sec)


SUB QUERIES:

1.Write a query to display all the orders from the orders table issued by the salesman 'Paul Adam'

mysql> select * from orders where salesman_id in(select salesman_id from salesman where name='Paul Adam');
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70011 |     75.29 | 2012-08-17 |        3003 |        5007 |
+--------+-----------+------------+-------------+-------------+
1 row in set (0.00 sec)


2.Write a query to display all the orders for the salesman who belongs to the city London

mysql> select * from orders where salesman_id in(select salesman_id from salesman where city='London');
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70009 |    270.65 | 2012-09-10 |        3001 |        5005 |
+--------+-----------+------------+-------------+-------------+
1 row in set (0.00 sec)


3.Write a query to find all the orders issued against the salesman who works for customer whose id is 3007


mysql> select * from orders where salesman_id not in(select salesman_id from customers where customer_id=3007);
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70001 |    150.50 | 2012-10-05 |        3005 |        5002 |
|  70009 |    270.65 | 2012-09-10 |        3001 |        5005 |
|  70004 |    110.50 | 2012-08-17 |        3009 |        5003 |
|  70007 |    948.50 | 2012-09-10 |        3005 |        5002 |
|  70010 |   1983.43 | 2012-10-10 |        3004 |        5006 |
|  70003 |   2480.40 | 2012-10-10 |        3009 |        5003 |
|  70012 |    250.45 | 2012-06-27 |        3008 |        5002 |
|  70011 |     75.29 | 2012-08-17 |        3003 |        5007 |
+--------+-----------+------------+-------------+-------------+
8 rows in set (0.00 sec)


4.Write a query to display the commission of all the salesmen servicing customers in Paris


mysql> select commission from salesman where city in(select city from customers where city='Paris');
+------------+
| commission |
+------------+
|       0.13 |
|       0.14 |
+------------+
2 rows in set (0.02 sec)












    